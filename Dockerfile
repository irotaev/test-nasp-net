FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY *.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY . ./
RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:2.2
WORKDIR /app
COPY --from=build-env /app/out .
COPY --from=build-env /app/aspnetcore-cert.pfx .
ENTRYPOINT ["dotnet", "aspnetwebapi.dll"]
#ENV Kestrel__Certificates__Default__Path aspnetcore-cert.pfx
#ENV Kestrel__Certificates__Default__Password 1111
#ENV ASPNETCORE_URLS http://+:5000;https://+:5001
ENV ASPNETCORE_URLS http://+:5000
EXPOSE 5000
#EXPOSE 5001